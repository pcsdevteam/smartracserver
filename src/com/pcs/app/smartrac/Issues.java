package com.pcs.app.smartrac;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import javax.ws.rs.core.Response;

@Path("/issues")
public class Issues {
  @GET
  @Produces("application/json")
  public Response issuesList() throws JSONException {

	JSONObject jsonObject = new JSONObject();
	JSONArray jsonlist = new JSONArray();
	jsonlist.put("TV  -  Sensor");
	jsonlist.put("TV  -  Adapter Loose connection");
	jsonlist.put("TV  -  Screen, Multi Color");
	jsonlist.put("AndroidBox - Adapter Loose connection");
	jsonlist.put("AndroidBox - Electric socket pin");
	jsonlist.put("Remotes - Replace");
	jsonlist.put("Remotes - Air mouse malfunctioning");
	jsonlist.put("Remotes - Batteries");
	jsonlist.put("Enclosure -  Bolts");
	jsonlist.put("Enclosure - Screws");
	jsonlist.put("Head phones -  Pin @ TV");
	jsonlist.put("Head phones - Pin @ passnger side");
	jsonlist.put("Software -App setting issue");
	jsonlist.put("Software -App Config issue");
	jsonlist.put("Infra - Outside-wires ( Power )");
	jsonlist.put("Infra - Outside-wires (  LAN )");
	jsonlist.put("Infra - Outside-wires (  RF cables )");
	jsonlist.put("Electrical - Inverter Issue");
	jsonlist.put("Content - Pen Drive issue"); 
	jsonlist.put("Content - Hard drive Drive issue");
	jsonObject.put("IssueTypes", jsonlist);
	String result = "@Produces(\"application/json\")" + jsonObject;
	return Response.status(200).entity(result).build();
  }
}
